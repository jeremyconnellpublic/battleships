﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleships.Interface
{
    public interface IBattleships
    {
        Guid CreateGame(string player1Id, string player1Name, string player2Id, string player2Name);
        EPlacement AddShip(Guid gameId, string playerId, List<Coordinates> ship);
        EAttack Attack(Guid gameId, string playerId, Coordinates location);
    }

    public enum EPlacement
    {
        Ok = 0,
        OffTheBoard = 1,
        Conflict = 2,
        NotInLine = 3,
        Empty = 4,
        InvalidGameId = 5,
        InvalidPlayerId = 6,
        GameAlreadyStarted = 7,
        InputDataIsNull = 8
    }
    public enum EAttack
    {
        Hit = 0,
        Miss = 1,
        GameOver = 2,
        NotYourTurn = 3,
        OffTheBoard = 4,
        InvalidGameId = 5,
        InvalidPlayerId = 6,
        NotReady = 7,
        InputDataIsNull = 8
    }

}
