﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleships.Interface
{
    public class Coordinates : IComparable<Coordinates>
    {
        //Data
        public int X { get; set; }
        public int Y { get; set; }

        //Sorting
        public int CompareTo(Coordinates other)
        {
            int i = X.CompareTo(other.X);
            if (0 == i)
                i = Y.CompareTo(other.Y);
            return i;
        }
    }
}
