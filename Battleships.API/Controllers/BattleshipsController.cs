﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using Battleships.DataAccess;
using Microsoft.Extensions.Hosting;
using Battleships.Interface;
using Battleships.API.Model;
using System.Collections.Generic;

namespace Battleships.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BattleshipsController : ControllerBase
    {
        private readonly IBattleships _battleships;

        public BattleshipsController(IHostEnvironment env)
        {
            string path = Path.Combine(env.ContentRootPath, "App_Data");
            IStorage sfs = new StorageFileSystem(path);
            _battleships = new BusinessLayer.Battleships(sfs);
        }

        [HttpGet]
        public string CreateGame(string player1Id, string player1Name, string player2Id, string player2Name)
        {
            try
            {
                return _battleships.CreateGame(player1Id, player1Name, player2Id, player2Name).ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpPut]
        public string AddShip(Guid gameId, string playerId, List<Coordinates> ship)
        {
            return _battleships.AddShip(gameId, playerId, ship).ToString();
        }

        [HttpPost]
        public string Attack(Guid gameId, string playerId, Coordinates shot)
        {
            return _battleships.Attack(gameId, playerId, shot).ToString();
        }


        //[HttpPut]
        //public string AddShip(AddShip x)
        //{
        //    return _battleships.AddShip(x.GameId, x.PlayerId, x.Ship).ToString();
        //}

        //[HttpPost]
        //public string Attack(Attack x)
        //{
        //    return _battleships.Attack(x.GameId, x.PlayerId, x.Shot).ToString();
        //}
    }
}
