﻿using Battleships.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Battleships.API.Model
{
    public class Attack
    {
        public Guid GameId;
        public string PlayerId;
        public Coordinates Shot;
    }
}
