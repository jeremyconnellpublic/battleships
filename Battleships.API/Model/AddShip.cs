﻿using System;
using System.Collections.Generic;
using Battleships.Interface;

namespace Battleships.API.Model
{
    public class AddShip
    {
        public Guid GameId;
        public string PlayerId;
        public List<Coordinates> Ship;
    }
}
