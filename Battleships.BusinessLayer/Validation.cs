﻿using System;
using Battleships.Interface;
using System.Collections.Generic;
using Battleships.DataAccess;

namespace Battleships.BusinessLayer
{
    public static class Validation
    {
        #region Game
        public static void Game(string player1Id, string player1Name, string player2Id, string player2Name)
        {
            //Null/Empty
            if (string.IsNullOrEmpty(player1Id))   throw new Exception("Invalid Player1Id");
            if (string.IsNullOrEmpty(player2Id))   throw new Exception("Invalid Player2Id");
            if (string.IsNullOrEmpty(player1Name)) throw new Exception("Invalid Player1Name");
            if (string.IsNullOrEmpty(player2Name)) throw new Exception("Invalid Player2Name");

            //Same player
            if (player1Id == player2Id)       throw new Exception("PlayerIds must be different");
        }
        public static bool GameHasStarted(Game game)
        {
            return game.HasStarted;
        }
        public static bool GameIsFair(Game game)
        {
            return game.IsFair;
        }
        public static bool IsNull(Guid gameId, string playerId, object obj)
        {
            if (Guid.Empty == gameId) return true;
            if (string.IsNullOrEmpty(playerId)) return true;
            return null == obj;
        }
        #endregion

        #region IsInLine
        public static bool IsInLine(List<Coordinates> position)
        {
            position.Sort();
            if (!IsInRow(position) && !IsInCol(position))
                return false;
            return IsSequential(position);                
        }
        private static bool IsInRow(List<Coordinates> position)
        {
            var first = position[0].Y;
            foreach (var i in position)
                if (i.Y != first)
                    return false;
            return true;
        }
        private static bool IsInCol(List<Coordinates> position)
        {
            var first = position[0].X;
            foreach (var i in position)
                if (i.X != first)
                    return false;
            return true;
        }
        private static bool IsSequential(List<Coordinates> position)
        {
            int prev = position[0].X + position[0].Y - 1;
            foreach (Coordinates i in position)
            {
                int x = i.X + i.Y;
                if (x != prev + 1)
                    return false;
                prev = x;
            }
            return true;
        }
        #endregion

        #region IsOnBoard
        public static bool IsOnBoard(List<Coordinates> position)
        {
            foreach (var i in position)
                if (!IsOnBoard(i))
                    return false;
            return true;
        }
        public static bool IsOnBoard(Coordinates i)
        {
            return i.X >= 0 && i.Y >= 0 && i.X <= 9 && i.Y <= 9;
        }
        #endregion

        #region IsUniqueLocation
        public static bool IsUniqueLocation(List<Coordinates> position, Board board)
        {
            foreach (var i in position)
                if (!IsUniqueLocation(i, board))
                    return false;
            return true;
        }
        private static bool IsUniqueLocation(Coordinates c, Board board)
        {
            foreach (var i in board.Ships)
                foreach (var j in i.Position)
                    if (c.X == j.X && c.Y == j.Y)
                        return false;
            return true;
        }
        #endregion
    }
}
