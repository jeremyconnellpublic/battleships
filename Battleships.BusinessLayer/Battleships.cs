﻿using System;
using System.Collections.Generic;
using Battleships.DataAccess;
using Battleships.Interface;

namespace Battleships.BusinessLayer
{
    public class Battleships : IBattleships
    {
        //Constructor - Initialise storage provider
        public Battleships(IStorage storage)
        {
            Storage.Provider = storage;
        }

        //Public - Interface
        Guid IBattleships.CreateGame(string player1Id, string player1Name, string player2Id, string player2Name)
        {
            //Throw exception for invalid inputs
            Validation.Game(player1Id, player1Name, player2Id, player2Name);

            //Create Game
            var game = new Game(player1Id, player1Name, player2Id, player2Name);
            Storage.Provider.Save(game);
            return game.GameId;
        }

        EPlacement IBattleships.AddShip(Guid gameId, string playerId, List<Coordinates> position)
        {
            //Validate not null
            if (Validation.IsNull(gameId, playerId, position))
                return EPlacement.InputDataIsNull;

            //Validate not empty
            if (position.Count == 0)
                return EPlacement.Empty;

            //Validate on the board
            if (!Validation.IsOnBoard(position))
                return EPlacement.OffTheBoard;

            //Validate in a line
            if (!Validation.IsInLine(position))
                return EPlacement.NotInLine;

            //Validate GameId
            Game game = Storage.Provider.Load(gameId);
            if (null == game)
                return EPlacement.InvalidGameId;

            //Validate Player
            Board board = game.GetBoard(playerId, true);
            if (null == board)
                return EPlacement.InvalidPlayerId;

            //Validate unique location
            if (!Validation.IsUniqueLocation(position, board))
                return EPlacement.Conflict;

            //Validate hasn't started
            if (Validation.GameHasStarted(game))
                return EPlacement.GameAlreadyStarted;

            //Create ship
            var ship = new Ship();
            foreach (var i in position)
                ship.Position.Add(new Coordinate() { X = i.X, Y = i.Y });
            board.Ships.Add(ship);
            Storage.Provider.Save(game);
            return EPlacement.Ok;
        }

        EAttack IBattleships.Attack(Guid gameId, string playerId, Coordinates location)
        {
            //Validate not null
            if (Validation.IsNull(gameId, playerId, location))
                return EAttack.InputDataIsNull;

            //Validate On The Board
            if (!Validation.IsOnBoard(location))
                return EAttack.OffTheBoard;

            //Validate GameId
            Game game = Storage.Provider.Load(gameId);
            if (null == game)
                return EAttack.InvalidGameId;

            //Validate Player
            Board board = game.GetBoard(playerId, false);
            if (null == board)
                return EAttack.InvalidPlayerId;

            //Validate Turn
            if (!game.IsMyTurn(playerId))
                return EAttack.NotYourTurn;

            //Validate is fair (even number of ships, non-zero)
            if (!game.HasStarted)
            {
                if (!Validation.GameIsFair(game))
                    return EAttack.NotReady;
                game.StartedUtc = DateTime.UtcNow;
            }

            //Evaluate Attack, togggle turn, save
            bool isHit = board.Attack(new Coordinate() { X = location.X, Y = location.Y });
            game.IsPlayer1Turn = !game.IsPlayer1Turn;
            if (board.GameOver)
                game.CompletedUtc = DateTime.UtcNow;
            Storage.Provider.Save(game);
            return isHit ? (board.GameOver ? EAttack.GameOver : EAttack.Hit) : EAttack.Miss;
        }

    }
}
