﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Battleships.DataAccess
{
    public class Board
    {
        //Data
        public List<Ship> Ships;
        public List<Coordinate> Missed;

        //Constuctor
        public Board()
        {
            Ships = new List<Ship>();
            Missed = new List<Coordinate>();
        }

        //Properties
        [JsonIgnore]
        public bool GameOver
        {
            get
            {
                if (Ships.Count == 0)
                    return false;
                foreach (var i in Ships)
                    if (i.Sunk == false)
                        return false;
                return true;
            }
        }
        [JsonIgnore]
        public int TotalShipSize
        {
            get
            {
                int total = 0;
                foreach (var i in Ships)
                    total += i.Size;
                return total;
            }
        }

        //Functions
        public bool Attack(Coordinate c)
        {
            foreach (var i in Ships)
                foreach (var j in i.Position)
                    if (j.X == c.X && j.Y == c.Y)
                    {
                        j.Hit = true;
                        j.AttackTimeUtc = DateTime.UtcNow;
                        return true;
                    }
            c.AttackTimeUtc = DateTime.UtcNow;
            this.Missed.Add(c);
            return false;
        }
    }
}
