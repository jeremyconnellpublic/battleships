﻿using System;
using System.IO;

namespace Battleships.DataAccess
{
    public class StorageFileSystem : Storage, IStorage
    {
        //Constructor
        public StorageFileSystem(string folderPath)
        {
            _folderPath = folderPath;
            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);
        }

        //Members
        private string _folderPath;

        //Public
        public Game Load(Guid gameId)
        {
            string filePath = FilePath(gameId);
            if (!File.Exists(filePath))
                return null;
            string json = File.ReadAllText(filePath);
            return Game.FromJson(json);
        }

        public void Save(Game game)
        {
            string json = game.ToJson();
            string filePath = FilePath(game.GameId);
            File.WriteAllText(filePath, json);
        }

        //Private
        private string FilePath(Guid g)
        {
            //Use base64 for shorter names (replacing 2 chars)
            string fileName = g.ToString() + ".json";
            return Path.Combine(_folderPath, fileName);
        }
    }
}
