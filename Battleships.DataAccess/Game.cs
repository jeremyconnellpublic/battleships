﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Battleships.DataAccess
{
    public class Game
    {
        //Data
        public Guid GameId;
        public Player Player1;
        public Player Player2;
        public Board Board1;
        public Board Board2;
        public DateTime CreatedUtc;
        public DateTime StartedUtc;
        public DateTime CompletedUtc;
        public bool IsPlayer1Turn;

        //Constructor
        public Game() { }
        public Game(string player1Id, string player1Name, string player2Id, string player2Name)
        {
            GameId = Guid.NewGuid();
            CreatedUtc = DateTime.UtcNow;

            Player1 = new Player() { Id = player1Id, Name = player1Name };
            Player2 = new Player() { Id = player2Id, Name = player2Name };

            Board1 = new Board();
            Board2 = new Board();
        }


        //Helpers
        public Board GetBoard(string playerId, bool myBoard)
        {
            if (string.IsNullOrEmpty(playerId))
                return null;
            if (playerId == Player1.Id)
                return myBoard ? Board1 : Board2;
            if (playerId == Player2.Id)
                return myBoard ? Board2 : Board1;
            return null;
        }
        public bool IsMyTurn(string playerId)
        {
            if (playerId == Player1.Id)
                return IsPlayer1Turn;
            return !IsPlayer1Turn;
        }
        [JsonIgnore]
        public bool IsFair
        {
            get
            {
                return Board1.TotalShipSize == Board2.TotalShipSize && Board1.Ships.Count > 0;
            }
        }
        [JsonIgnore]
        public bool HasStarted
        {
            get
            {
                return StartedUtc != DateTime.MinValue;
            }
        }


        //Serialisation
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
        public static Game FromJson(string s)
        {
            return JsonConvert.DeserializeObject<Game>(s);
        }
    }
}
