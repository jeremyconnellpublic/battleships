﻿using System;

namespace Battleships.DataAccess
{
    public interface IStorage
    {
        void Save(Game game);
        Game Load(Guid gameId);
    }

    public abstract class Storage
    {
        //General Access (application needs to set)
        public static IStorage Provider { get; set; }
    }
}
