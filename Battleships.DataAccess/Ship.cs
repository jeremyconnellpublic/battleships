﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Battleships.DataAccess
{
    public class Ship
    {
        //Data
        public List<Coordinate> Position;
        public DateTime PlacedUtc;

        //Constructor
        public Ship()
        {
            Position = new List<Coordinate>();
            PlacedUtc = DateTime.UtcNow;
        }

        //Properties
        [JsonIgnore]
        public int Size {  get { return Position.Count; } }
        [JsonIgnore]
        public bool Sunk
        {
            get
            {
                foreach (var i in Position)
                    if (i.Hit == false)
                        return false;
                return true;
            }
        }
    }
}
