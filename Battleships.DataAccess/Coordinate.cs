﻿using System;

namespace Battleships.DataAccess
{
    public class Coordinate
    {
        public int X;
        public int Y;
        public bool Hit;
        public DateTime AttackTimeUtc;
    }
}
